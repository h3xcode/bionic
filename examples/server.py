# Copyright 2020-2021 h3xcode
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Example bionic server
"""

import asyncio
import logging
from bionic.models import Result
from bionic.server import BionicServer


logging.basicConfig(level=logging.INFO)

server = BionicServer()


@server.call_handler("max")
async def handler_srv1(_, data):
    """Call handler
    Get max number"""
    return Result(max(*data.params))


async def server_loop():
    """Example server"""
    await server.wait_connection("test")  # wait for client with label "test"
    while True:
        print("----------")
        print("Sum:", await server.send_call(
            "test",  # client label
            "add",  # method
            [1, 2],  # args
            return_only_result=True  # return result.result
        ))
        print("Multiply:", await server.send_call("test", "mul", [1, 2], return_only_result=True))
        await asyncio.sleep(2)


# start listening on 8122 (default) port
asyncio.ensure_future(server.start_server())

# start server loop
asyncio.get_event_loop().run_until_complete(server_loop())
