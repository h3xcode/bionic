# Copyright 2020-2021 h3xcode
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Example bionic client
"""


import logging
import asyncio
from bionic import client, models

client = client.BionicClient("test")  # "test" is a client label
logging.basicConfig(level=logging.INFO)


@client.call_handler("add")  # handler for method "add"
async def handler_add(_, data: models.Call):
    """Example call handler
    Add two numbers"""
    return models.Result(data.params[1] + data.params[0])


@client.call_handler("mul")  # handler for method "mul"
async def handler_mul(_, data: models.Call):
    """Example call handler
    Multiply two numbers"""
    return models.Result(data.params[1] * data.params[0])

# # or (instead wrappers)
# client.add_call_handler("add", handler_add)
# client.add_call_handler("mul", handler_mul)


async def client_loop():
    """Example client"""
    # connect to server (8122 - default port)
    await client.connect("127.0.0.1", 8122)
    while True:
        print(
            await client.send_call(
                "max",  # method
                [1, 2, 3],  # params (list or dict)
                return_only_result=True,  # return result.result
                raise_on_error=True  # raise exception on error
            ))
        await asyncio.sleep(2)

# start client loop
asyncio.get_event_loop().run_until_complete(client_loop())
